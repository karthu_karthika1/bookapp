package com.ust.bookapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ust.bookapp.entity.Book;



public interface BookRepository extends JpaRepository<Book, Integer>{
}
