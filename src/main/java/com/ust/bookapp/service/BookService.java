package com.ust.bookapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.bookapp.entity.Book;
import com.ust.bookapp.repo.BookRepository;



@Service
public class BookService {
	@Autowired
	private BookRepository repo;
	
	public Book saveBook(Book book) {
		return repo.save(book);
	}
	
	public List<Book> getAllBooks(){
		List<Book> findAllBooks = (List<Book>) repo.findAll();
		return findAllBooks;
		
	}
	public void deleteBook(Integer bid) {
		repo.deleteById(bid);
	}
	public Book updateBook(Book book) {
		Integer BId= book.getBid();
		Book bk= repo.findById(BId).get();
		bk.setBname(book.getBname());
		bk.setBauthor(book.getBauthor());
		bk.setBcategory(book.getBcategory());
		bk.setBimg(book.getBimg());
		bk.setBprice(book.getBprice());
		bk.setBavailability(book.getBavailability());
		return repo.save(bk);
	
		
	}
	
}
