package com.ust.bookapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ust.bookapp.entity.Book;
import com.ust.bookapp.service.BookService;



@RestController

public class BookController {

	@Autowired
	private BookService service;
	
	
	 @PostMapping(path= "/addbooks")
	 @CrossOrigin(origins = "http://localhost:4200")
	 public Book register(@RequestBody Book book) throws Exception{
		 Book bookObj = null;
		 bookObj= service.saveBook(book);
		 return bookObj;
	 }
		@GetMapping(path="/books")
		@CrossOrigin(origins ="http://localhost:4200")
		public List<Book> getAllBooks(){
			List<Book> allBooks = service.getAllBooks();
	        return allBooks;
		}
		@DeleteMapping("/books/{bid}")
		@CrossOrigin(origins ="http://localhost:4200")
		public ResponseEntity<String> delete(@PathVariable("bid") int bid) {
			service.deleteBook(bid);
			return new ResponseEntity<String>("Book is deleted successfully.!", HttpStatus.OK);
		}
		@PutMapping("/books")
		@CrossOrigin(origins ="http://localhost:4200")
	    public Book updateBook(@RequestBody Book book) {
			return service.updateBook(book);
		}
		
	
	
	@GetMapping("/lists")
	public String getMessages() {
		return "list of books";
	}
	
}
